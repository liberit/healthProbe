\subsection{/dev/sda      LITEON CS1-SP32-11 M.2 2242 32GB}
\begin{description}
\item[capacity]/dev/sda2 26G\\
/dev/sda1 3.9G\\
\item[used]/dev/sda2 59%\\
/dev/sda1 8%\\
\item[test time] 0.17 hours
\item[health test]
Completed successfully.
\item[storage age] 0.0055 years.
\end{description}
\subsection{/dev/sdb      HGST HTS541010A7E630}
\begin{description}
\item[capacity]/dev/sdb3 70G\\
/dev/sdb7 851G\\
\item[used]/dev/sdb3 55%\\
/dev/sdb7 85%\\
\item[test time] 3.70 hours
\item[health test]
Completed successfully.
\item[storage age] 0.3833 years.
\end{description}
