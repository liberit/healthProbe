#!/bin/bash
# harddrive check:
#   smartctl short, and long
#     for $(ls /dev/sd* |grep /dev/sd.$)
#  ./storageProbe.sh &
#        smartctl -t long
#   check for errors, and completion
#   check for age of hard-drive and make of hard-drive
#   if make is Seagate, recommend RAID
#   if age is greater than 4yrs, recommend RAID
# cpu check:
#   check percentage used

function stressy() {
  STRESS="$(stress "$2" "$1" -t 60)"
  echo "$STRESS";
#if [[ $(echo "$STRESS"|grep -c unsuccessful) == 0 ]] 
#then
#echo "$STRESS" | grep successful | sed -e 's/.*]//'
#else
#echo "ERROR!"
#echo "$STRESS" 
#fi
}

{
  CORES=$(nproc --all);
  THREADS=$((CORES*2));
echo '\chapter{Health Probes}'
echo '\section{CPU}'
echo '\begin{description}'
echo '\item[Number of CPU Cores]'
echo "$CORES"
echo '\item[CPU]'
stressy "$THREADS" -c
echo '\item[Input/Output]'
stressy "$THREADS" -i
echo '\end{description}'
echo '\section{Memory}'
echo '\begin{description}'
echo '\item[Virtual Memory]'
stressy "$THREADS" -m
echo '\end{description}'
echo '\section{Storage}'
echo '\begin{description}'
echo '\item[Read/Write]'
stressy "$THREADS" -d
echo '\end{description}'
} | tee tmp/"${HOSTNAME}"-healthprobe.tex

# gpu check:
#   stress test with something
# ram check:
#   check percentage free
#   annual boot-time stress test

# make sure RAM/CPU/HDD usage <= 80% otherwise recommend an extension. 
# if greater than 60% give it an OKAY, if less than 60% usage give it a GOOD.

# can have a recurring CPU/RAM percentage check, about every half hour
# then take the top 20% of values and average them. 

# generate montly PDF based on results



