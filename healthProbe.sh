#!/bin/bash
# harddrive check:
#   smartctl short, and long
#     for $(ls /dev/sd* |grep /dev/sd.$)
#  ./storageProbe.sh &
#        smartctl -t long
#   check for errors, and completion
#   check for age of hard-drive and make of hard-drive
#   if make is Seagate, recommend RAID
#   if age is greater than 4yrs, recommend RAID
# cpu check:
#   check percentage used

function stressy() {
STRESS="$(sudo stress-ng --sequential "$1" --class "$2" -t 60s --times\
   --metrics-brief)"
if [[ $(echo "$STRESS"|grep -c unsuccessful) == 0 ]] 
then
echo "$STRESS" | grep successful | sed -e 's/.*]//'
else
echo "ERROR!"
echo "$STRESS" 
fi
}

{
CORES=$(nproc --all);
echo '\chapter{Health Probes}'
echo '\section{CPU}'
echo '\begin{description}'
echo '\item[Number of CPU Cores]'
echo "$CORES"
echo '\item[CPU]'
stressy "$CORES" cpu
echo '\item[CPU cache (hidden memory)]'
stressy "$CORES" cpu-cache
echo '\item[Input/Output]'
stressy "$CORES" io
echo '\item[Pipe]'
stressy "$CORES" pipe
echo '\end{description}'
echo '\section{Memory}'
echo '\begin{description}'
echo '\item[Memory]'
stressy "$CORES" memory
echo '\item[Virtual Memory]'
stressy "$CORES" vm
echo '\end{description}'
echo '\section{Operating System}'
echo '\begin{description}'
echo '\item[OS]'
stressy "$CORES" os
echo '\item[Interrupt]'
stressy "$CORES" interrupt
echo '\item[Scheduler]'
stressy "$CORES" scheduler
echo '\item[File System]'
stressy "$CORES" filesystem
echo '\item[Network]'
stressy "$CORES" network
echo '\end{description}'
} | tee tmp/"${HOSTNAME}"-healthprobe.tex

# gpu check:
#   stress test with something
# ram check:
#   check percentage free
#   annual boot-time stress test

# make sure RAM/CPU/HDD usage <= 80% otherwise recommend an extension. 
# if greater than 60% give it an OKAY, if less than 60% usage give it a GOOD.

# can have a recurring CPU/RAM percentage check, about every half hour
# then take the top 20% of values and average them. 

# generate montly PDF based on results



