#!/bin/bash
function driveCheck() {
  if [[ $(/usr/sbin/smartctl -i "$1"|grep -c "SMART support is: Enabled") == 1 ]];  
    then 
    TIME=$(/usr/sbin/smartctl -c "$1"|grep -i extended -A 1|grep time| \
      awk '{print $5}' |cut -d ')' -f 1);
    DEVICE=$(/usr/sbin/smartctl -i "$1" |grep Device.Model \
      | awk -F ":" '{print $2}');
    {

    echo '\subsection{'"$1" "$DEVICE"'}'
    COMPLETE=$(/usr/sbin/smartctl -a "$1" | grep -c '^#\ 1.*Completed\ without\ error')
    echo '\begin{description}'
    if [ -d "/cygdrive" ] 
    then
      echo '\item[capacity]'"$(df -h|grep ":/"| awk '{print $1 " " $2 "\\\\"}')"
      echo '\item[used]'"$(df -h|grep ":/"| awk '{print $1 " " $5 "\\\\" }')"
    else 
      echo '\item[capacity]'"$(df -h|grep "$1"| awk '{print $1 " " $2 "\\\\"}')"
      echo '\item[used]'"$(df -h|grep "$1"| awk '{print $1 " " $5 "\\\\" }')"
    fi
    echo '\item[test time]' "$(python -c "print(\"%.2f\" % ($TIME/60.0))")" hours;
    /usr/sbin/smartctl -q errorsonly -t long "$1"
    #sleep "$TIME"m
    echo '\item[health test]'
    if [[ $COMPLETE == 1 ]] 
      then echo "Completed successfully.";
      else 
        echo "Error encountered!:"'\\'
        echo '\begin{lstlisting}'
        /usr/sbin/smartctl -a "$1" | grep  ^#\ 1
        echo '\end{lstlisting}'
    fi
    UPTIME=$(python -c "print(\"%.4f\" % ($(/usr/sbin/smartctl -a "$1"|grep ^#\ 2\
      |sed -e 's/.*%//'| awk '{ print $1}') /24/365.25))")
    echo  '\item[storage age]' "$UPTIME" years.
    echo '\end{description}'
    } | tee -a out.txt
    fi; 

}
rm out.txt
{
echo '\chapter{'"$(hostname)"'}'
echo '\section{Storage}'
export -f  driveCheck
find /dev/sd* -regextype grep -regex /dev/sd.$ \
  -exec bash -c 'driveCheck "$0" ' {} \;
} | tee tmp/"${HOSTNAME}"-storage.tex
